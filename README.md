# 🎨 Colors
Username color picker bot for Discord, written in Python using nextcord.

## Requirements
* Python3
* Linux system with systemd
* Discord bot (with intents for messages enabled)

## Installation

Clone the repository, go in it, install nextcord, copy the template config file, edit it and run the script.

```bash
git clone https://codeberg.org/botaddicts/colors
cd colors/
pip install -r requirements.txt
cp config.template.py config.py
nano config.py
python3 main.py
```

And for systemd: (running in the background):

```bash
sed -i "s|ABSOLUTE_PATH|$(pwd)|g" colors.service
sed -i "s|USER|$USER|g" colors.service
sudo cp colors.service /etc/systemd/system/
sudo systemctl start colors
```

## Usage
To do
