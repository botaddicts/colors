import nextcord, datetime
from config import token, ids
from nextcord.ext import commands

import logging

logging.basicConfig(level=logging.INFO)

intents = nextcord.Intents.default()
intents.members = True
bot = nextcord.Client()


@bot.slash_command(description="Set a colour code", guild_ids=ids)
async def set(interaction, code: str = nextcord.SlashOption(name="colour", description="Hex colour code", required=True)):
    colour = nextcord.Colour(int(code, 16))

    # Get role of user or create it
    role = nextcord.utils.get(interaction.guild.roles, name=f"USER-{interaction.user.id}")
    position = nextcord.utils.get(interaction.guild.roles, name="colors_above").position

    if role == None:
        role = await interaction.guild.create_role(name=f"USER-{interaction.user.id}", colour=colour)
        await role.edit(position=position)
    else:
        await role.edit(colour=colour, position=position)

    await interaction.user.add_roles(role)
    embed = nextcord.Embed(
        colour=colour, title="New color assigned", description=f"`#{code}`"
    )
    embed.set_thumbnail(url=f"https://via.placeholder.com/50/{code}/{code}.png")
    await interaction.response.send_message("Done!", embed=embed)


@bot.slash_command(description="Unset your custom colour", guild_ids=ids)
async def unset(interaction):
    role = nextcord.utils.get(interaction.guild.roles, name=f"USER-{interaction.user.id}")
    await interaction.user.remove_roles(role)
    embed = nextcord.Embed(
        title="Your colour has been reset",
        description="You can set one again using `%set`",
    )
    await interaction.response.send_message("Your colour has been reset", embed=embed)


bot.run(token)
